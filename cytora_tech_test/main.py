from typing import Dict
from cytora_tech_test.entities import Condition, Rule, UnevaluatedStatementsException


def evaluate(rule: Rule, data: Dict[str, int]) -> bool:
    """
    Checks that a rule is valid for a given input.

    Usage:
        rule = Rule(
            statements=[
                Condition(field="foo", predicate=Predicate.less_than, target=50), 
                Condition(field="bar", predicate=Predicate.greater_than, target=10),
                Condition(field="baz", predicate=Predicate.equals, target=2),
            ], 
            operator=Operator.OR
        )

        input = {
            "foo": 10,
            "bar": 15,
            "baz": 2
        }

        evaluate(rule, input)
    """

    stack = [rule]

    while stack:
        rule_or_condition = stack.pop()

        if isinstance(rule_or_condition, Condition):
            condition = rule_or_condition
            value = data[condition.field]
            condition.evaluate_condition(value)
        
        elif isinstance(rule_or_condition, Rule):
            rule = rule_or_condition
            statements = rule.statements

            try: 
                rule.evaluate_rule()
            except UnevaluatedStatementsException:
                # NOTE: Its important that we put the rule back on the 
                # stack before the statements, since the loop pops from
                # the back of the stack and we want to evaluate the statements
                # before the rule
                stack.append(rule)
                stack.extend(statements)

    return rule.result