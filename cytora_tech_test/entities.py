from dataclasses import dataclass
from enum import Enum
from typing import List, Optional, Union


class Predicate(Enum):
    """
    A predicate is a mathematical operation which can
    be peformed on on two values which has a true or false answer.
    """
    greater_than = "GREATER_THAN"
    less_than = "LESS_THAN"
    equals = "EQUALS"

    def func(self):
        operations = {
            Predicate.greater_than: lambda value, target: value > target,
            Predicate.less_than: lambda value, target: value < target,
            Predicate.equals: lambda value, target: value == target
        }

        return operations[self]


@dataclass
class Condition:
    """
    A condition is a constraint on a field which is either true or false when 
    evaluated against a target value.

    Only `integer` fields and target values are supported.
    """
    field: str
    predicate: Predicate
    target: int
    result: Optional[bool] = None

    def is_evaluated(self):
        return self.result is not None

    def evaluate_condition(self, value: int):
        func = self.predicate.func()
        self.result = func(value, self.target)


class Operator(Enum):
    """
    An mathematical operator which can be applied to a a list of boolean (or truthy/falsey) values. 
    """
    AND = "AND"
    OR = "OR"

    def func(self):
        operations = {
            Operator.AND: lambda items: all(items),
            Operator.OR: lambda items: any(items)
        }

        return operations[self]


class UnevaluatedStatementsException(Exception):
    """There is at least 1 statement on the `Rule` which has not been evaluated."""


@dataclass
class Rule:
    """
    A rule is a set of statements, each of which evaluates to true or false.

    A statement can be a `Condition` but it can also be another `Rule`.

    There is not enforced limit on the size/dimension of a `Rule`.
    """

    statements: List[Union[Condition, "Rule"]]
    operator: Operator
    result: Optional[bool] = None

    def is_evaluated(self):
        return self.result is not None
    
    def evaluate_rule(self):
        """
        Evaluate the result of a rule.

        A Rule can only be evaluated if all of its statements have been evaluated.

        - raises: UnevaluatedStatementsException - At least 1 statement on the rule 
        has not been evaluated.
        """
        all_statements_evaluated = all([s.is_evaluated() for s in self.statements])

        if not all_statements_evaluated:
            raise UnevaluatedStatementsException
    
        operations = {
            Operator.AND: lambda condition_results: all(condition_results),
            Operator.OR: lambda condition_results: any(condition_results)
        }

        func = operations[self.operator]
        self.result = func([s.result for s in self.statements])