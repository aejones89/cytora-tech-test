# Cytora Tech Test

## Requirements

This project uses [Poetry](https://python-poetry.org/) to manage the dependencies and local dev environment. Make sure you have Poetry installed.

## Run

Run the tests:

```
poetry run python -m unittest discover
```

Run the program interactively from the shell:

```
poetry shell
```

Then import the `evaluate` function, create rules and evaluate them against test data. (See [the tests](./tests/test_evaluate.py) for examples.)

## Assumptions

- There is no limit of the size of a rule. Since a rule contains other rules, a rule could theoretically be infinitely large. To keep things simple we assume that we will never be handling rules with 100s or 1000s levels of nesting.
- We also assume that the rules are not big-data type objects (i.e. not huge GB-sized files). We can safely hold these in the runtime memory, there is no risk of these consuming all our available memory.
- Only integer values are supported in the conditions and values. There are no checks in the code in-case a non-`int` is used. We place no guarantees on the types and need to be respected by the developers using this code.
- A field value must be a string - we allow anything (eg. the tests use `"foo"`, `"bar"` and `"baz"` - the code does not check or allow specific values at runtime).

## Discussion

- Possible extension: We could enforce only certain fields in our code - eg: Only allow `credit_rating`, `flood_risk` and `revenue`. If we want to allow other fields they have to be explicitly opted into. We could extend our code to enforce this at runtime. Fields could be statically defined in code or fetched at run or compile-time from a data-store.

- If I were told that the rules have some max limit in size and are only ever going to be nested 5-10 layers deep, I might consider refactoring my solution to use recursion instead. Of course it's personal preference but I find the recursive solution slightly easier to read and to grok - but we have to be careful with recursion in python unless we have a strong guarantee on the limit/size of our data structures.
