import unittest

from cytora_tech_test import evaluate
from cytora_tech_test.entities import Condition, Operator, Predicate, Rule


class NestedTestCase(unittest.TestCase):

    @staticmethod
    def rule():
        return Rule(
            statements=[
                Rule(
                    statements=[
                        Condition(field="credit_rating", predicate=Predicate.greater_than, target=50), 
                        Condition(field="flood_risk", predicate=Predicate.less_than, target=10)
                    ], 
                    operator=Operator.AND
                ), 
                Condition(field="revenue", predicate=Predicate.greater_than, target=1000000)
            ], 
            operator=Operator.OR
        )

    def test_passes(self):
        input = {
            "credit_rating": 75,
            "flood_risk": 5,
            "revenue": 1000
        }

        rule = self.rule()

        assert evaluate(rule, input) == True

    def test_fails(self):
        input = {
            "credit_rating": 40,
            "flood_risk": 15,
            "revenue": 1000
        }

        rule = self.rule()

        assert evaluate(rule, input) == False

    def test_corner_case(self):
        input = {
            "credit_rating": 50,
            "flood_risk": 10,
            "revenue": 1000000
        }

        rule = self.rule()

        assert evaluate(rule, input) == False


class SimpleWithAndTestCase(unittest.TestCase):

    @staticmethod
    def rule():
        return Rule(
            statements=[
                Condition(field="foo", predicate=Predicate.less_than, target=50), 
                Condition(field="bar", predicate=Predicate.greater_than, target=10),
                Condition(field="baz", predicate=Predicate.equals, target=2),
            ], 
            operator=Operator.AND
        )

    def test_passes(self):
        input = {
            "foo": 10,
            "bar": 15,
            "baz": 2
        }

        rule = self.rule()

        assert evaluate(rule, input) == True

    def test_fails_if_one_condition_not_met(self):
        input = {
            # the "foo" contition will fail
            "foo": 60,
            "bar": 15,
            "baz": 2
        }

        rule = self.rule()

        assert evaluate(rule, input) == False

    def test_corner_case(self):
        input = {
            "foo": 50,
            "bar": 10,
            "baz": 2
        }

        rule = self.rule()

        assert evaluate(rule, input) == False


class SimpleWithOrTestCase(unittest.TestCase):

    @staticmethod
    def rule():
        return Rule(
            statements=[
                Condition(field="foo", predicate=Predicate.less_than, target=50), 
                Condition(field="bar", predicate=Predicate.greater_than, target=10),
                Condition(field="baz", predicate=Predicate.equals, target=2),
            ], 
            operator=Operator.OR
        )

    def test_passes(self):
        input = {
            # Only "foo" is valid
            "foo": 10,
            "bar": 5,
            "baz": 3
        }

        rule = self.rule()

        assert evaluate(rule, input) == True

    def test_fails_if_all_conditions_not_met(self):
        input = {
            "foo": 60,
            "bar": 5,
            "baz": 3
        }

        rule = self.rule()

        assert evaluate(rule, input) == False